<?php

namespace Database\Seeders;

use App\Models\TipoRecurso;
use Illuminate\Database\Seeder;

class TipoRecursoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TipoRecurso::create([
            'descripcion' => '.rar'
        ]);
        TipoRecurso::create([
            'descripcion' => '.zip'
        ]);
        TipoRecurso::create([
            'descripcion' => '.pdf'
        ]);
        TipoRecurso::create([
            'descripcion' => '.doc'
        ]);
    }
}
