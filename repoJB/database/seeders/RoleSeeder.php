<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;


class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roleAdmin = Role::create(['name' => 'Admin']);
        $roleAlumno = Role::create(['name' => 'Alumno']);
        $roleProfesor = Role::create(['name' => 'Profesor']);

        Permission::create(['name' => 'admin.users.index'])->syncRoles([$roleAdmin]);
        Permission::create(['name' => 'admin.users.edit'])->syncRoles([$roleAdmin]);
        Permission::create(['name' => 'admin.users.update'])->syncRoles([$roleAdmin]);

        Permission::create(['name' => 'ciclos.index'])->syncRoles([$roleAdmin, $roleAlumno, $roleProfesor]);
        Permission::create(['name' => 'ciclos.show'])->syncRoles([$roleAdmin, $roleAlumno, $roleProfesor]);

        Permission::create(['name' => 'proyectos.create'])->syncRoles([$roleAdmin, $roleAlumno, $roleProfesor]);
        Permission::create(['name' => 'proyectos.edit'])->syncRoles([$roleAdmin, $roleAlumno, $roleProfesor]);
        Permission::create(['name' => 'proyectos.index'])->syncRoles([$roleAdmin, $roleAlumno, $roleProfesor]);
        Permission::create(['name' => 'proyectos.personales'])->syncRoles([$roleAdmin, $roleAlumno, $roleProfesor]);
        Permission::create(['name' => 'proyectos.show'])->syncRoles([$roleAdmin, $roleAlumno, $roleProfesor]);
    }
}
