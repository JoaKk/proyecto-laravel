<?php

namespace Database\Seeders;

use App\Models\Ciclo;
use App\Models\Comentario;
use App\Models\Proyecto;
use App\Models\Recurso;
use App\Models\TipoRecurso;
use App\Models\User;
use App\Models\Version;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class ProyectoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $proyectos = Proyecto::factory(40)->create();
        foreach ($proyectos as $proyecto) {
            Storage::makeDirectory('recursos/' . $proyecto->id);
            Recurso::create([
                'proyecto_id' => $proyecto->id,
                'ruta_fichero' => 'recurso/'.$proyecto->id.'/default.rar',
                'tipo_id' => TipoRecurso::all()->random()->id
            ]);
            Recurso::create([
                'proyecto_id' => $proyecto->id,
                'ruta_fichero' => 'recurso/'.$proyecto->id.'/default.pdf',
                'tipo_id' => TipoRecurso::all()->random()->id
            ]);
            Version::factory(2)->create([
                'proyecto_id' => $proyecto->id
            ]);
            Comentario::factory(1)->create([
                'proyecto_id' => $proyecto->id,
                'user_id' => User::all()->random()->id
            ]);
            Comentario::factory(1)->create([
                'proyecto_id' => $proyecto->id,
                'user_id' => User::all()->random()->id
            ]);
            Comentario::factory(1)->create([
                'proyecto_id' => $proyecto->id,
                'user_id' => User::all()->random()->id
            ]);
        }
    }
}
