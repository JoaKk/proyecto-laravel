<?php

namespace Database\Seeders;

use App\Models\Ciclo;
use App\Models\Instituto;
use Illuminate\Database\Seeder;

class CicloSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ciclos = Ciclo::factory(20)->create();

        foreach ($ciclos as $ciclo){
            $ciclo->institutos()->attach([
                Instituto::all()->skip(0)->take(5)->random()->id,
                Instituto::all()->skip(5)->take(5)->random()->id
            ]);
        }
    }
}
