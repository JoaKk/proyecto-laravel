<?php

namespace Database\Seeders;

use App\Models\Instituto;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Admin Repo',
            'slug' => 'Admin-Repo',
            'email' => 'admin@admin.com',
            'password' => bcrypt('admin'),
            'publico' => 1,
            'localidad' => 'Cantabria',
            'instituto_id' => Instituto::all()->random()->id,
            'profile_photo_path' => 'https://picsum.photos/200'
        ])->assignRole('Admin');
        User::factory(40)->create();
    }
}
