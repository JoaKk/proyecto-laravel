<?php

namespace Database\Factories;

use App\Models\Ciclo;
use App\Models\Proyecto;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ProyectoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Proyecto::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $nombre = $this->faker->unique()->sentence();
        return [
            'nombre' => $nombre,
            'publico' => $this->faker->boolean,
            'slug' => Str::slug($nombre),
            'user_id' => User::all()->random()->id,
            'descargas' => $this->faker->randomNumber(3),
            'valoracion' => $this->faker->randomFloat(1,1,5),
            'ciclo_id' => Ciclo::all()->random()->id, 
            'descripcion' => $this->faker->text()
        ];
    }
}
