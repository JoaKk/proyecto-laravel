@extends('adminlte::page')

@section('title', 'Inicio')

@section('content_header')
    <div>
        <h1 class="text-center inline-block">
            Bienvenidos al
            <strong>Repositorio</strong>
            <p class="text-xl" style="letter-spacing: -4pt;white-space: nowrap;">
                <span style="color: red">J</span>
                <span style="color: black">B</span>
            </p>
        </h1>
    </div>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-primary">
                <div class="inner">
                    <h3>COMPARTE</h3>
                    <p>Sube a la nube tus proyectos</p>
                </div>
                <div class="icon">
                    <i class="fas fa-arrow-circle-up"></i>
                </div>
                <a href="{{route('proyectos.create')}}" class="small-box-footer">Más información <i class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
                <div class="inner">
                    <h3>EXPLORA</h3>

                    <p>Busca proyectos de otros usuarios</p>
                </div>
                <div class="icon">
                    <i class="fas fa-search"></i>
                </div>
                <a href="{{route('proyectos.index')}}" class="small-box-footer">Más información <i class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
                <div class="ribbon-wrapper ribbon-xl">
                    <div class="ribbon bg-danger text-lg">
                        En desarrollo
                    </div>
                </div>
                <div class="inner">
                    <h3>GESTIONA</h3>

                    <p>Organiza las tareas de tu proyecto</p>
                </div>
                <div class="icon">
                    <i class="fas fa-calendar-alt"></i>
                </div>
                <a href="#" class="small-box-footer">Más información <i class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
            <!-- small box -->

            <div class="small-box bg-danger">
                <div class="ribbon-wrapper ribbon-xl">
                    <div class="ribbon bg-warning text-lg">
                        En desarrollo
                    </div>
                </div>
                <div class="inner">
                    <h3>EJECUTA</h3>

                    <p>Despliega cualquier aplicación</p>
                </div>
                <div class="icon">
                    <i class="fas fa-cog"></i>
                </div>
                <a href="#" class="small-box-footer">Más información <i class="fas fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
    </div>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    
@stop
