@extends('adminlte::page')

@section('title', 'Admin - Editar Usuario')

@section('content')
    <div class="card">
        <div class="card-header bg-dark p-0 py-2">
            <section class="content-header">
                <h3 class="card-title"><span class="fas fa-fx fa-users"></span> Editar usuario</h3>
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a class="text-white" href="{{ route('inicio') }}">Inicio</a></li>
                    <li class="breadcrumb-item"><a class="text-white" href="{{route('admin.users.index')}}">Usuarios</a></li>
                    <li class="breadcrumb-item active text-white-50">Editar usuario</li>
                </ol>
            </section>
        </div>
        <div class="card-body">
            <p class="h5">Nombre del usuario:</p>
            <p class="form-control">{{$user->name}}</p>
            <h2 class="h5">Listado de roles</h2>
            {!! Form::model($user, ['route' => ['admin.users.update',$user], 'method' => 'put']) !!}
                @foreach ($roles as $role)
                    <div>
                        <label>
                            {!! Form::checkbox('roles[]', $role->id,null, ['class' => 'mr-1']) !!}
                            {{$role->name}}
                        </label>
                    </div>
                @endforeach
                {!! Form::submit('Asignar Rol', ['class' => 'btn btn-primary mt-2']) !!}
            {!! Form::close() !!}
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
@if (session('info'))
    <script>
        Swal.fire(
            'Actualizado!',
            'Se han actualizado los roles del usuario con éxito.',
            'success'
        )
    </script>
    @endif
@stop