<div>
    <div class="card">
        <div class="card-header bg-info p-0 py-2">
            <section class="content-header">
                <h3 class="card-title"><span class="fab fa-fx fa-buffer"></span> Lista de ciclos</h3>
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a class="text-white" href="{{ route('inicio') }}">Inicio</a></li>
                    <li class="breadcrumb-item active text-white-50">Ciclos</li>
                </ol>
            </section>
        </div>
        <div class="card-header">
            <div class="form-group">
                <div class="input-group input-group-lg">
                    <input wire:model="search" type="search" class="form-control form-control-lg"
                        placeholder="Inserte el nombre del ciclo">
                    <div class="input-group-append">
                        <span class="btn btn-lg btn-default">
                            <i class="fa fa-search"></i>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        @if ($ciclos->count())
            <div class="card-body">
                <table class="table table-striped" id="users">
                    <thead>
                        <tr>
                            <th style="width: 10%">ID</th>
                            <th style="width: 50%">Ciclo Formativo</th>
                            <th style="width: 30%"></th>
                            <th style="width: 20%"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($ciclos as $ciclo)
                            <tr>
                                <td>{{ $ciclo->id }}</td>
                                <td>{{ $ciclo->nombre }}</td>
                                <td></td>
                                <td>
                                    <a class="btn btn-primary" href="{{ route('ciclos.show', $ciclo) }}">Ver detalle</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                {{ $ciclos->links() }}
            </div>
        @else
            <div class="card-body">
                <strong>No hay ningún registro...</strong>
            </div>
        @endif
    </div>
</div>
