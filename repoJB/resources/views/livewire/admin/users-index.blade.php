<div>
    <div class="card">
        <div class="card-header bg-dark p-0 py-2">
            <section class="content-header">
                <h3 class="card-title"><span class="fas fa-fx fa-users"></span> Lista de usuarios</h3>
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a class="text-white" href="{{ route('inicio') }}">Inicio</a></li>
                    <li class="breadcrumb-item active text-white-50">Usuarios</li>
                </ol>
            </section>
        </div>
        <div class="card-body">
            <table class="table table-striped" id="users">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Email</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $user)
                        <tr>
                            <td>{{$user->id}}</td>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>
                                <a class="btn btn-primary" href="{{route('admin.users.edit',$user)}}">Editar</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
