<div>
    <div class="card">
        <div class="card-header bg-success p-0 py-2">
            <section class="content-header">
                <h3 class="card-title"><span class="fas fa-fx fa-list"></span> Mis proyectos</h3>
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a class="text-white" href="{{ route('inicio') }}">Inicio</a></li>
                    <li class="breadcrumb-item active text-white-50">Mis proyectos</li>
                </ol>
            </section>
        </div>
        <div class="card-header">
            <div class="form-group">
                <div class="input-group input-group-lg">
                    <input wire:model="busqueda" type="search" class="form-control form-control-lg"
                        placeholder="Inserte el nombre del proyecto">
                    <div class="input-group-append">
                        <span class="btn btn-lg btn-default">
                            <i class="fa fa-search"></i>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        @if ($proyectos->count())
            <div class="card-body p-0">
                <table class="table table-striped projects">
                    <thead>
                        <tr>
                            <th style="width: 11%">
                                Autor
                            </th>
                            <th style="width: 20%">
                                Nombre del proyecto
                            </th>
                            <th>
                                Progreso del proyecto
                            </th>
                            <th>
                                Ciclo Formativo
                            </th>
                            <th style="width: 8%" class="text-center">
                                Estado
                            </th>
                            <th style="width: 1%">
                            </th>
                            <th style="width: 10%">
                            </th>
                            <th style="width: 8%">
                            </th>
                            <th style="width: 10%">
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($proyectos as $proyecto)
                            <tr>
                                <td>
                                    <ul class="list-inline">
                                        <li class="list-inline-item">
                                            <img alt="Avatar" class="table-avatar"
                                                src="{{ $proyecto->user->profile_photo_path }}">
                                        </li>
                                    </ul>
                                </td>
                                <td>
                                    <a href="{{ route('proyectos.show', $proyecto) }}">
                                        {{ $proyecto->nombre }}
                                    </a>
                                    <br />
                                    <small>
                                        Publicado el {{ $proyecto->created_at }}
                                    </small>
                                </td>
                                <td class="project_progress">
                                    <div class="progress progress-sm">
                                        @php
                                            $random = rand(0, 100);
                                        @endphp
                                        @if ($random < 25)
                                            <div class="progress-bar bg-success progress-bar-striped progress-bar-animated"
                                                role="progressbar" aria-valuenow="{{ $random }}" aria-valuemin="0"
                                                aria-valuemax="100" style="width:{{ $random }}%">
                                            </div>

                                        @elseif ($random < 50) <div
                                                class="progress-bar bg-info progress-bar-striped progress-bar-animated"
                                                role="progressbar" aria-valuenow="{{ $random }}" aria-valuemin="0"
                                                aria-valuemax="100" style="width:{{ $random }}%">
                                    </div>

                                @elseif ($random < 75) <div
                                        class="progress-bar bg-warning progress-bar-striped progress-bar-animated"
                                        role="progressbar" aria-valuenow="{{ $random }}" aria-valuemin="0"
                                        aria-valuemax="100" style="width:{{ $random }}%">
            </div>

        @elseif ($random <= 100) <div class="progress-bar bg-danger progress-bar-striped progress-bar-animated"
                role="progressbar" aria-valuenow="{{ $random }}" aria-valuemin="0" aria-valuemax="100"
                style="width:{{ $random }}%">
    </div>
    @endif
</div>
<small>
    {{ $random }}% Completado
</small>
</td>
<td>
    <i>{{ $proyecto->ciclo->nombre }}</i>
</td>
<td class="project-state">
    @if ($proyecto->publico)
        <span class="badge badge-success">Público</span>
    @else
        <span class="badge badge-danger">Privado</span>
    @endif
</td>
<td></td>
<td class="project-actions text-right">
    <a class="btn btn-primary btn-sm" href="{{ route('proyectos.show', $proyecto) }}">
        <i class="fas fa-eye">
        </i>
        Ver proyecto
    </a>
</td>
<td>
    <a class="btn btn-info btn-sm" href="{{route('proyectos.edit',$proyecto)}}">
        <i class="fas fa-pencil-alt">
        </i>
        Editar
    </a>
</td>
<td>
    <form class="formulario-eliminar" action="{{ route('proyectos.destroy', $proyecto) }}" method="POST">
        @csrf
        @method('DELETE')
        <button class="btn btn-danger btn-sm" type="submit"><i class="fas fa-trash">
            </i>Eliminar</button>
    </form>
</td>
</tr>
@endforeach
</tbody>
</table>
</div>
<div class="card-footer">
    {{ $proyectos->links() }}
</div>
@else
<div class="card-body">
    <strong>No tienes nada registrado... &#128524; ¡Ánimate y sube tu proyecto personal! &#128515;</strong>
</div>
@endif
</div>

</div>
