@extends('adminlte::page')

@section('title', 'Proyectos')

@section('content')
@livewire('proyectos-index')
@stop

@section('css')
    <link rel="stylesheet" href="/css/app.css">
@stop

@section('js')

@stop
