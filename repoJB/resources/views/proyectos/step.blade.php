@extends('adminlte::page')

@section('title', 'Step')

@section('content')
    <div class="card">
        <div class="card-header bg-primary p-0 py-2">
            <section class="content-header">
                <h3 class="card-title"><span class="fas fa-fx fa-upload"></span> Sube tu proyecto a nuestro repositorio</h3>
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a class="text-white" href="{{ route('inicio') }}">Inicio</a></li>
                    <li class="breadcrumb-item active text-white-50">Sube tu proyecto</li>
                </ol>
            </section>
        </div>
        <div class="card-body card-body-step">
            <div class="progress-bar-step">
                <div class="step">
                    <p>Información</p>
                    <div class="bullet">
                        <span></span>
                    </div>
                    <div class="check fas fa-check"></div>
                </div>
                <div class="step">
                    <p>Logo</p>
                    <div class="bullet">
                        <span></span>
                        <span class="line"></span>
                    </div>
                    <div class="check fas fa-check"></div>
                </div>
                <div class="step">
                    <p>Archivos</p>
                    <div class="bullet">
                        <span></span>
                    </div>
                    <div class="check fas fa-check"></div>
                </div>
                <div class="step">
                    <p>Subir</p>
                    <div class="bullet">
                        <span></span>
                    </div>
                    <div class="check fas fa-check"></div>
                </div>
            </div>
            <div class="form-outer">
                {!! Form::open(['route' => 'proyectos.store', ' autocomplete' => 'off', 'files' => true]) !!}
                @csrf
                <div class="page slide-page">
                    <div class="title"><i class="fas fa-info-circle"></i> Información del proyecto</div>
                    <div class="field">
                        {!! Form::label('nombre', 'Título del proyecto:', ['class' => 'label']) !!}
                        {!! Form::text('nombre', null, [
                        'class' => 'form-control',
                        'placeholder' => 'Inserte el título del proyecto',
                        ]) !!}
                    </div>
                    <div class="field-error">
                        @error('nombre')
                            <small class="text-danger label-error">{{ $message }}</small>
                        @enderror
                    </div>
                    <hr>
                    <div class="field" style="display: none">
                        {!! Form::label('slug', 'Slug:', ['class' => 'label']) !!}
                        {!! Form::text('slug', null, [
                        'class' => 'form-control',
                        'placeholder' => 'Aquí se mostrará el slug del proyecto',
                        'readonly',
                        ]) !!}
                    </div>
                    <div class="field-error" style="display: none">
                        @error('slug')
                            <small class="text-danger label-error">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="row">
                        <div class="col form-group">
                            <div class="field">
                                {!! Form::label('ciclo_id', 'Ciclo Formativo:', ['class' => 'label']) !!}
                                <br>
                                {!! Form::select('ciclo_id', $ciclos, null, [
                                'class' => 'js-example-basic-single form-control',
                                ]) !!}
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="from-group">
                        <div class="field">
                            {!! Form::label('descripcion', 'Descripción del proyecto:', ['class' => 'label']) !!}
                            {!! Form::textarea('descripcion', null, ['class' => 'form-control', 'rows' => '3', 'placeholder'
                            => 'Introduce una breve descripción de tu proyecto']) !!}
                        </div>
                    </div>
                    <div class="field-error">
                        @error('descripcion')
                            <small class="text-danger label-error">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="field firstNext">
                        <button>Siguiente</button>
                    </div>
                </div>

                <div class="page">
                    <div class="title mb-15"><i class="fas fa-image"></i> Logo del proyecto</div>
                    <div class="row">
                        <div class="col">
                            <div class="image-wrapper">
                                @isset($proyecto->ruta_logo)
                                    <img id="imagenLogo" src="{{ Storage::url($proyecto->ruta_logo) }}" alt="">
                                @else
                                    <img id="imagenLogo" src="{{ Storage::url('default.jpg') }}" alt="">
                                @endisset
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                {!! Form::file('ruta_logo', ['class' => 'form-control-file', 'accept' => 'image/*']) !!}

                            </div>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi dignissimos tempora
                                voluptate, quaerat
                                perspiciatis exercitationem atque. Omnis adipisci necessitatibus minima? Culpa delectus
                                molestiae eligendi!
                                At dolores deserunt numquam asperiores amet.</p>
                        </div>
                        <hr>
                    </div>

                    @error('ruta_logo')
                        <small class="text-danger">{{ $message }}</small>
                    @enderror
                    <div class="field btns">
                        <button class="prev-1 prev">Atrás</button>
                        <button class="next-1 next">Siguiente</button>
                    </div>
                </div>

                <div class="page">
                    <div class="title"><i class="fas fa-folder-open"></i> Carpeta del proyecto</div>

                    <div class="form group">
                        {!! Form::label('ruta_fichero', 'Carpeta contenedora del proyecto') !!}
                        <small>(Tipo de archivo válido : <strong>.rar o .zip</strong>)</small>
                        {!! Form::file('ruta_fichero', ['class' => 'form-control-file', 'accept' => '.rar,.zip']) !!}

                    </div>
                    <div class="field-error">
                        @error('ruta_fichero')
                            <small class="text-danger label-error">{{ $message }}</small>
                        @enderror
                    </div>
                    <hr>
                    <div class="title"><i class="fas fa-file-pdf"></i> Documentación del proyecto</div>

                    <div class="form group">
                        {!! Form::label('ruta_documentacion', 'Documentación del proyecto') !!}
                        <small>(Tipo de archivo válido : <strong>.pdf o .doc</strong>)</small>
                        {!! Form::file('ruta_documentacion', ['class' => 'form-control-file', 'accept' => '.pdf,.doc']) !!}
                    </div>
                    <div class="field-error">
                        @error('ruta_documentacion')
                            <small class="text-danger label-error">{{ $message }}</small>
                        @enderror
                    </div>
                    <hr>
                    <div class="field btns">
                        <button class="prev-2 prev">Atrás</button>
                        <button class="next-2 next">Siguiente</button>
                    </div>
                </div>

                <div class="page">
                    <div class="title">¡Ya queda poco!</div>
                    <div class="field">
                        <div class="label">
                            <span>¿Quieres compartir tu proyecto con los demás usuarios?</span>
                        </div>
                    </div>
                    <div class="field">
                        <div class="label">
                            Tu proyecto será :
                            <input type="checkbox" name="publico[]" checked data-toggle="toggle" data-on="Público"
                                data-off="Privado" data-onstyle="success" data-offstyle="danger" data-height="40"
                                data-width="105">
                        </div>
                    </div>
                    <div class="field">
                        <div class="label">
                            Pulsa <strong>"SUBIR PROYECTO"</strong> para almacenar tu proyecto personal
                            en nuestro repositorio
                        </div>
                    </div>
                    <div class="field-error">
                        @error('publico')
                            <small class="text-danger label-error">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="field btns">
                        <button class="prev-3 prev">Atrás</button>
                        {!! Form::submit('Subir proyecto', ['class' => 'submit btn']) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop

@section('css')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/app.css">
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css"
        rel="stylesheet">
@stop

@section('js')
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
    <script src="{{ asset('vendor/jQuery-Plugin-stringToSlug-1.3/jquery.stringToSlug.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="/js/script.js"></script>
    <script>
        $(document).ready(function() {
            $("#nombre").stringToSlug({
                setEvents: 'keyup keydown blur',
                getPut: '#slug',
                space: '-'
            });
            $('.js-example-basic-single').select2();

        });

    </script>
    <script>
        //Cambiar imagen
        document.getElementById("ruta_logo").addEventListener('change', cambiarImagen);

        function cambiarImagen(event) {
            var file = event.target.files[0];

            var reader = new FileReader();
            reader.onload = (event) => {
                document.getElementById("imagenLogo").setAttribute('src', event.target.result);
            };

            reader.readAsDataURL(file);
        }

    </script>
@stop
