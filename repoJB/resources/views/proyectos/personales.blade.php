@extends('adminlte::page')

@section('title', 'Mis proyectos')

@section('content')
    @livewire('proyectos-personales')
@stop

@section('css')
    <link rel="stylesheet" href="/css/app.css">
@stop

@section('js')
    <!--<script> TRAER DATOS AL SCRIPT
                        const user = {!!  json_encode($proyectos) !!};
                        console.log(user);
                    </script>-->

    @if (session('destroyInfo'))
        <script>
            Swal.fire(
                '¡Eliminado!',
                'Tu proyecto ha sido eliminado con éxito.',
                'success'
            )

        </script>
    @endif
    <script>
        $('.formulario-eliminar').submit(function(e) {
            e.preventDefault();

            Swal.fire({
                title: '¿Está usted seguro?',
                text: "Si lo haces no hay vuelta atrás!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '¡Sí, eliminar!',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.isConfirmed) {
                    this.submit();
                }
            })
        });

    </script>
@stop
