@extends('adminlte::page')

@section('title', 'Editar Proyecto')

@section('content')
    <div class="card">
        <div class="card-header bg-primary p-0 py-2">
            <section class="content-header">
                <h3 class="card-title"><span class="fas fa-fx fa-pencil-alt"></span> Edita tu proyecto personal</h3>
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a class="text-white" href="{{ route('inicio') }}">Inicio</a></li>
                    <li class="breadcrumb-item active text-white-50">Edita tu proyecto</li>
                </ol>
            </section>
        </div>
        <div class="card-body">
            {!! Form::model($proyecto, ['route' => ['proyectos.update', $proyecto], ' autocomplete' => 'off', 'files' =>
            true, 'method' => 'put']) !!}
            @csrf

            @include('proyectos.layouts.form')

            {!! Form::submit('Actualizar proyecto', ['class' => 'btn btn-primary']) !!}
            {!! Form::close() !!}
        </div>
    </div>
@stop
@section('css')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="/css/app.css">
@stop

@section('js')
    <script src="{{ asset('vendor/jQuery-Plugin-stringToSlug-1.3/jquery.stringToSlug.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="https://cdn.ckeditor.com/ckeditor5/25.0.0/classic/ckeditor.js"></script>
    <script>
        $(document).ready(function() {
            $("#nombre").stringToSlug({
                setEvents: 'keyup keydown blur',
                getPut: '#slug',
                space: '-'
            });
            $('.js-example-basic-single').select2();
        });

    </script>
    <script>
        ClassicEditor
            .create(document.querySelector('#descripcion'), {
                language: 'es',
            })
            .catch(error => {
                console.error(error);
            });

    </script>
    <script>
        //Cambiar imagen
        document.getElementById("ruta_logo").addEventListener('change', cambiarImagen);

        function cambiarImagen(event) {
            var file = event.target.files[0];

            var reader = new FileReader();
            reader.onload = (event) => {
                document.getElementById("imagenLogo").setAttribute('src', event.target.result);
            };

            reader.readAsDataURL(file);
        }

    </script>
    @if (session('updateInfo'))
    <script>
        Swal.fire(
            'Actualizado!',
            'Tu proyecto ha sido actualizado con éxito.',
            'success'
        )
    </script>
    @endif
@stop
