@extends('adminlte::page')

@section('title', 'Detalle Proyecto')

@section('content')

    <div class="card">
        <div class="card-header bg-success p-0 py-2">
            <section class="content-header">
                <h3 class="card-title">
                @if ($proyecto->publico)<span class="fas fa-lock-open"> </span> @else
                        <span class="fas fa-lock"> </span>
                    @endif {{ $proyecto->nombre }}
                    <small class="ml-5"><i>Actualizado el {{ $proyecto->updated_at }}</i></small>
                </h3>
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a class="text-white" href="{{ route('inicio') }}">Inicio</a></li>
                    <li class="breadcrumb-item"><a class="text-white" href="{{ route('proyectos.index') }}">Lista de
                            proyectos</a></li>
                    <li class="breadcrumb-item active text-white-50">Detalle del proyecto</li>
                </ol>
            </section>
        </div>
        <div class="card-body">
            @if ($proyecto->publico)
                <div class="row">
                    <div class="col-12 col-md-12 col-lg-8 order-2 order-md-1">
                        <div class="row">
                            <div class="col-12">
                                <h4 class="mb-4">Linea temporal del proyecto</h4>
                                <div class="timeline">
                                    @php
                                        $fechaTimeline = null;
                                        if($proyecto->versiones->last() != null){
                                            $fechaPrimeraVersion = $proyecto->versiones->last()->created_at->format('d-F-Y');
                                        }else{
                                            $fechaPrimeraVersion = null;
                                        }
                                    @endphp
                                    @foreach ($proyecto->versiones as $version)
                                        @if ($fechaTimeline != $version->created_at->format('d-F-Y'))
                                            <div class="time-label">
                                                <span class="bg-red">{{ $version->created_at->format('d-F-Y') }}</span>
                                            </div>
                                            @php
                                                $fechaTimeline = $version->created_at->format('d-F-Y');
                                            @endphp
                                        @endif
                                        <div>
                                            <i class="fas fa-code-branch bg-blue"></i>
                                            <div class="timeline-item">
                                                <span class="time"><i class="fas fa-clock"></i>
                                                    {{ $version->created_at->format('H:i') }}</span>
                                                <h3 class="timeline-header"><a href="#">Versión </a><b>
                                                        {{ $version->nombre }}</b>
                                                </h3>

                                                <div class="timeline-body">
                                                    {{ $version->descripcion }}
                                                </div>
                                                <div class="timeline-footer">

                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                    @if ($fechaPrimeraVersion != $proyecto->created_at->format('d-F-Y'))
                                    <div class="time-label">
                                        <span class="bg-red">{{ $proyecto->created_at->format('d-F-Y') }}</span>
                                    </div>
                                    @endif
                                    <div>
                                        <i class="fas fa-cloud-upload-alt bg-yellow"></i>
                                        <div class="timeline-item">
                                            <span class="time"><i class="fas fa-clock"></i>
                                                {{ $proyecto->created_at->format('H:i') }}</span>
                                            <h3 class="timeline-header"><b>Subida del proyecto al repositorio</b>
                                            </h3>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-12">
                                <h4>Comentarios</h4>
                                @if ($proyecto->comentarios->count())
                                    <div class="card pt-2">

                                        @foreach ($proyecto->comentarios as $comentario)
                                            <div class="post ml-2">
                                                <div class="row">
                                                    <div class="user-block col">
                                                        <a href="{{ route('perfil', $comentario->getUsuario()) }}"><img
                                                                class="img-circle img-bordered-sm"
                                                                src="{{ $comentario->getUsuario()->adminlte_image() }}"
                                                                alt="user image"></a>
                                                        <div>
                                                            <span class="username">
                                                                <a
                                                                    href="{{ route('perfil', $comentario->getUsuario()) }}">{{ $comentario->getUsuario()->name }}</a>
                                                            </span>
                                                            <span class="description"><i class="far fa-clock"></i> Publicado
                                                                el
                                                                {{ $comentario->created_at }}</span>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        @if ($comentario->user_id == auth()->user()->id)
                                                            <div style="top:0;right: 0;">
                                                                <button
                                                                    class="comentario-eliminar btn btn-danger float-right"
                                                                    data-href="{{ route('proyectos.eliminarComentario', $comentario) }}">
                                                                    <i class="far fa-trash-alt"></i>
                                                                </button>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                                <p>
                                                    {{ $comentario->descripcion }}
                                                </p>
                                            </div>
                                        @endforeach
                                    </div>
                                @else
                                    <div class="card pt-2">
                                        <div class="row">
                                            <p class="text-center m-auto">
                                                <strong>¡Sé el primero en comentar este proyecto!</strong>
                                            </p>
                                        </div>
                                    </div>
                                @endif

                            </div>
                            <form action="{{ route('proyectos.comentar', $proyecto) }}" method="post"
                                class="input-group input-group-sm mb-0 ml-2">
                                @csrf
                                <input name="comentario" class="form-control form-control-sm"
                                    placeholder="Escribir comentario">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-danger">Enviar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-12 col-md-12 col-lg-4 order-1 order-md-2">
                        <h3 class="text-primary">{{ $proyecto->nombre }}</h3>
                        <img class="h-80 w-100" style="border: 5px black solid"
                            src="{{ Storage::url($proyecto->ruta_logo) }}" alt="">
                        <br>
                        <div class="text-muted">
                            <p class="text-md"><i class="fas fa-file-alt"></i> Descripción del proyecto :
                                <b class="text-muted text-md">{!! $proyecto->descripcion !!}</b>
                            </p>
                        </div>
                        <br>
                        <div class="text-muted">

                            <p class="text-md"><i class="fas fa-user"></i> Autor/a :
                                <a href="{{ route('perfil', $proyecto->user) }}">
                                    <b>{{ $proyecto->user->name }}</b>
                                </a>
                            </p>

                            <p class="text-md"><i class="fas fa-graduation-cap"></i> Ciclo Formativo :
                                <b>{{ $proyecto->ciclo->nombre }}</b>
                            </p>
                            <p class="text-md"><i class="fas fa-fx fa-school"></i> Instituto :
                                <b>{{ $proyecto->user->instituto->nombre }}</b>
                            </p>
                            <p class="text-md"><i class="fas fa-fx fa-cloud-download-alt"></i> Descargas :
                                <b>{{ $proyecto->descargas }}</b>
                            </p>
                        </div>

                        <h5 class="mt-5 text-muted">Archivos del proyecto</h5>
                        <ul class="list-unstyled">
                            <li>
                                <a href="" class="btn-link text-secondary"><i class="far fa-fw fa-file-word"></i>
                                    Functional-requirements.docx</a>
                            </li>
                            <li>
                                <a href="" class="btn-link text-secondary"><i class="far fa-fw fa-file-pdf"></i> UAT.pdf</a>
                            </li>
                            <li>
                                <a href="" class="btn-link text-secondary"><i class="far fa-fw fa-envelope"></i>
                                    Email-from-flatbal.mln</a>
                            </li>
                            <li>
                                <a href="{{ route('proyectos.descargarLogo', $proyecto) }}"
                                    class="btn-link text-secondary"><i class="far fa-fw fa-image "></i> Logo.png</a>
                            </li>
                            <li>
                                <a href="{{ route('proyectos.descargarLogo', $proyecto) }}"
                                    class="btn-link text-secondary"><i class="far fa-fw fa-file-word"></i>
                                    Contract-10_12_2014.docx</a>
                            </li>
                        </ul>
                        <div class="text-center mt-5 mb-3">
                            <a href="{{ route('proyectos.download', $proyecto) }}"
                                class="btn btn-sm btn-primary">Descargar
                                Proyecto</a>
                            <a href="#" class="btn btn-sm btn-warning disabled">Ejecutar proyecto</a>
                        </div>
                    </div>
                </div>
            @else
                <div class="alert alert-danger" role="alert">
                    <span><i class="fas fa-lock"></i> Este proyecto es <strong>PRIVADO</strong></span>
                </div>
            @endif
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/app.css">
@stop

@section('js')
    @if (session('createInfo'))
        <script>
            Swal.fire(
                '¡Genial!',
                'Ya tenemos tu proyecto en nuestro repositorio.',
                'success'
            )

        </script>
    @endif
    @if (session('eliminarComentarioInfo'))
        <script>
            Swal.fire(
                '¡Comentario eliminado!',
                '',
                'success'
            )

        </script>
    @endif
    <script>
        $('.comentario-eliminar').click(function(e) {
            e.preventDefault();

            Swal.fire({
                title: '¿Desea eliminar el comentario?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '¡Sí, eliminar!',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.isConfirmed) {
                    thisdata = $(this).attr('data-href');
                    window.location.href = thisdata;
                }
            })
        });

    </script>
@stop
