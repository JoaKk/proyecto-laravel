<div class="form-group">
    {!! Form::label('nombre', 'Título del proyecto:') !!}
    {!! Form::text('nombre', null, [
    'class' => 'form-control',
    'placeholder' => 'Inserte el título del proyecto',
    ]) !!}
    <hr>
    @error('nombre')
        <small class="text-danger">{{ $message }}</small>
    @enderror
</div>
<div class="form-group">
    {!! Form::label('slug', 'Slug:') !!}
    {!! Form::text('slug', null, [
    'class' => 'form-control',
    'placeholder' => 'Aquí se mostrará el slug del proyecto',
    'readonly',
    ]) !!}
    <hr>
    @error('slug')
        <small class="text-danger">{{ $message }}</small>
    @enderror
</div>
<div class="row">


    <div class="col form-group">
        {!! Form::label('ciclo_id', 'Ciclo Formativo:') !!}
        <br>
        {!! Form::select('ciclo_id', $ciclos, null, ['class' => 'js-example-basic-single form-control ']) !!}
    </div>
    <div class="col form-group border-left">
        <p class="font-weight-bold">Estado del proyecto: </p>
        <label>
            {!! Form::radio('publico', 1, true) !!}
            Público
        </label>
        <label>
            {!! Form::radio('publico', 0, true) !!}
            Privado
        </label>
        @error('publico')
            <small class="text-danger">{{ $message }}</small>
        @enderror
    </div>
</div>
<hr>
<div class="form-group">
    {!! Form::label('descripcion', 'Descripción del proyecto:') !!}
    {!! Form::textarea('descripcion', null, ['class' => 'form-control', 'rows' => '3']) !!}
</div>
<hr>
@error('descripcion')
    <small class="text-danger">{{ $message }}</small>
@enderror
<div class="row mb-3">
    <div class="col">
        <div class="image-wrapper">
            @isset($proyecto->ruta_logo)
                <img id="imagenLogo" src="{{ Storage::url($proyecto->ruta_logo) }}" alt="">
            @else
                <img id="imagenLogo" src="{{ Storage::url('default.jpg') }}" alt="">
            @endisset

        </div>
    </div>
    <div class="col">
        <div class="form-group">
            {!! Form::label('ruta_logo', 'Logo del proyecto') !!}
            {!! Form::file('ruta_logo', ['class' => 'form-control-file', 'accept' => 'image/*']) !!}
        </div>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi dignissimos tempora voluptate, quaerat
            perspiciatis exercitationem atque. Omnis adipisci necessitatibus minima? Culpa delectus molestiae eligendi!
            At dolores deserunt numquam asperiores amet.</p>
    </div>
</div>
<hr>
@error('ruta_logo')
    <small class="text-danger">{{ $message }}</small>
@enderror
<div class="form group">
    {!! Form::label('ruta_fichero', 'Carpeta contenedora del proyecto') !!}
    <small>(Tipo de archivo válido : <strong>.rar o .zip</strong>)</small>
    {!! Form::file('ruta_fichero', ['class' => 'form-control-file', 'accept' => '.rar,.zip']) !!}
    <hr>
    @error('ruta_fichero')
        <small class="text-danger">{{ $message }}</small>
    @enderror
</div>
<div class="form group">
    {!! Form::label('ruta_documentacion', 'Documentación del proyecto') !!}
    <small>(Tipo de archivo válido : <strong>.pdf o .doc</strong>)</small>
    {!! Form::file('ruta_documentacion', ['class' => 'form-control-file', 'accept' => '.pdf,.doc']) !!}
    <hr>
    @error('ruta_documentacion')
        <small class="text-danger">{{ $message }}</small>
    @enderror
</div>
