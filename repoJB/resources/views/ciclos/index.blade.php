@extends('adminlte::page')

@section('title', 'Ciclos')

@section('content')
@livewire('ciclos-index')
@stop

@section('css')
    <link rel="stylesheet" href="/css/app.css">
@stop

@section('js')
    <script>
        console.log('Hi!');
    </script>
@stop
