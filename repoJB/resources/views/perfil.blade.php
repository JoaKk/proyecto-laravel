@extends('adminlte::page')

@section('title', 'Perfil')
@section('content')
    <div class="card card-widget widget-user shadow">
        <div class="widget-user-header bg-info">
            <div style="top:0;right:0;">
                <button class="btn btn-warning float-right">
                    <i class="fas fa-edit"></i>
                </button>
            </div>
            <h3 class="widget-user-username">{{ $user->name }}</h3>
            <h5 class="widget-user-desc">{{ $user->adminlte_desc() }}</h5>
        </div>
        <div class="widget-user-image">
            <img class="img-circle elevation-2" src="{{ $user->adminlte_image() }}" alt="User Avatar">
        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col-sm-4 border-right">
                    <div class="description-block">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title text-muted">Proyectos personales</h3>
                                <h3><span class="badge badge-warning float-right">{{ $user->proyectos->count() }}</span>
                                </h3>
                            </div>

                            @if ($user->proyectos->count())
                                <div id="accordion">
                                    @foreach ($user->proyectos as $proyecto)

                                        <div class="card card-gray collapsed-card ml-3 mr-3 mt-3">
                                            <div class="card-header row">
                                                <h3 class="col-sm-11 card-title">{{ $proyecto->nombre }}</h3>

                                                <div class="col-sm-1 card-tools">
                                                    <button type="button" class="btn btn-tool"
                                                        data-card-widget="collapse"><i class="fas fa-plus"></i>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="card-body" style="display: none;">
                                                {!! $proyecto->descripcion !!}
                                                <br>
                                                <small><i class="fas fa-graduation-cap"> Ciclo formativo :</i>
                                                    {{ $proyecto->ciclo->nombre }}</small>
                                                <br>
                                                <small><i class="fas fa-fx fa-cloud-download-alt"> Descargas :</i>
                                                    {{ $proyecto->descargas }}</small>
                                                <br>
                                                @if ($proyecto->publico)
                                                    <small><i class="fas fa-lock-open"> Público</i></small>
                                                    <br>
                                                @else
                                                    <small><i class="fas fa-lock"> Privado</i></small>
                                                    <br>
                                                @endif
                                                <a href="{{ route('proyectos.show', $proyecto) }}"
                                                    class="btn btn-warning mb-1 mt-2">Ver proyecto</a>
                                            </div>
                                        </div>

                                    @endforeach
                                </div>
                            @else
                                <div class="card pt-2 mt-2 ml-2 mr-2">
                                    <p class="text-center m-auto">
                                        <strong>Este usuario no tiene ningún proyecto subido</strong>
                                    </p>
                                </div>
                            @endif

                        </div>
                    </div>
                </div>
                <div class="col-sm-4 border-right">
                    <div class="description-block">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title text-muted">Información del perfil</h3>
                            </div>
                            <div class="card-body">
                                <div class="info-box shadow-sm">
                                    <span class="info-box-icon bg-danger"><i class="fas fa-cloud-upload-alt"></i></span>
                                    <h3><span class="badge bg-danger"></span></h3>
                                    <div class="info-box-content">
                                        <span class="info-box-text"><strong>Proyectos personales</strong></span>
                                        <span class="info-box-number text-muted">{{ $user->proyectos->count() }}</span>
                                    </div>
                                </div>
                                <hr>
                                <div class="info-box shadow-sm">
                                    <span class="info-box-icon bg-danger"><i class="fas fa-envelope"></i></span>
                                    <h3><span class="badge bg-danger"></span></h3>
                                    <div class="info-box-content">
                                        <span class="info-box-text"><strong>Email</strong></span>
                                        <span class="info-box-number text-muted">{{ $user->email }}</span>
                                    </div>
                                </div>
                                <hr>
                                <div class="info-box shadow-sm">
                                    <span class="info-box-icon bg-danger"><i class="fas fa-map-marker-alt"></i></span>
                                    <h3><span class="badge bg-danger"></span></h3>
                                    <div class="info-box-content">
                                        <span class="info-box-text"><strong>Localidad</strong></span>
                                        <span class="info-box-number text-muted">{{ $user->localidad }}</span>
                                    </div>
                                </div>
                                <hr>
                                <div class="info-box shadow-sm">
                                    <span class="info-box-icon bg-danger"><i class="fas fa-pencil-alt"></i></span>
                                    <h3><span class="badge bg-danger"></span></h3>
                                    <div class="info-box-content">
                                        <span class="info-box-text"><strong>Sobre mí</strong></span>
                                        <span class="info-box-number text-muted">Lorem ipsum dolor sit amet, consectetur
                                            adipisicing elit. Qui atque labore numquam magnam sunt dolore laboriosam, odit
                                            hic voluptas quae exercitationem expedita consectetur, molestias iusto, aliquid
                                            quibusdam et ex molestiae.</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="description-block">
                        <h5 class="description-header">PENSAR QUE PONER AQUÍ</h5>
                        <span class="description-text">MÁS DE LO MISMO</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script>
        console.log('Hi!');

    </script>
@stop
