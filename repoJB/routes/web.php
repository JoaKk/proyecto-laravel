<?php

use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\CicloController;
use App\Http\Controllers\PerfilController;
use App\Http\Controllers\ProyectoController;
use Illuminate\Support\Facades\Route;
use App\Models\Proyecto;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (auth()->user()) {
        return view('index');
    }
    return view('welcome');
})->name('home');

Route::middleware(['auth:sanctum', 'verified'])->get('ciclos', [CicloController::class, 'index'])->name('ciclos.index');

Route::middleware(['auth:sanctum', 'verified'])->get('ciclos/{ciclo}', [CicloController::class, 'show'])->name('ciclos.show');

Route::middleware(['auth:sanctum', 'verified'])->get('proyectos/mis-proyectos', [ProyectoController::class, 'personales'])->name('proyectos.personales');

Route::middleware(['auth:sanctum', 'verified'])->get('proyectos/step', [ProyectoController::class, 'step'])->name('proyectos.step');

Route::middleware(['auth:sanctum', 'verified'])->post('proyectos/{proyecto}/comentar', [ProyectoController::class, 'comentar'])->name('proyectos.comentar');

Route::middleware(['auth:sanctum', 'verified'])->get('proyectos/{proyecto}/eliminarComentario', [ProyectoController::class, 'eliminarComentario'])->name('proyectos.eliminarComentario');

Route::resource('proyectos', ProyectoController::class)->names('proyectos')->middleware(['auth:sanctum', 'verified']);

Route::middleware(['auth:sanctum', 'verified'])->get('proyectos/{proyecto}/descargar', [ProyectoController::class, 'download'])->name('proyectos.download');

Route::middleware(['auth:sanctum', 'verified'])->get('proyectos/{proyecto}/descargarLogo', [ProyectoController::class, 'descargarLogo'])->name('proyectos.descargarLogo');

Route::middleware(['auth:sanctum', 'verified'])->get('perfil/{user}', [PerfilController::class, 'index'])->name('perfil');

/*Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');*/
