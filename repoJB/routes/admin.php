<?php

use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\UserController;
use Illuminate\Support\Facades\Route;

Route::get('',[HomeController::class,'index'])->name('inicio');

Route::resource('users',UserController::class)->only(['index','edit','update'])->names('admin.users');