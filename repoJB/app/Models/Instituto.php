<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Instituto extends Model
{
    protected $table = 'institutos';
    use HasFactory;

    public function ciclos(){
        return $this->belongsToMany(Ciclo::class);
    }

    public function users(){
        return $this->hasMany(User::class);
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }   
}
