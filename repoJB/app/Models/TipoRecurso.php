<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TipoRecurso extends Model
{
    protected $table = 'tipos_recursos';
    use HasFactory;

    public function recursos(){
        return $this->hasMany(Recurso::class);
    }
}
