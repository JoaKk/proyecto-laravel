<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Proyecto extends Model
{
    protected $table = 'proyectos';
    protected $guarded = ['id','create_at','update_at'];
    use HasFactory;

    public function getRouteKeyName()
    {
        return 'slug';
    }   

    public function recursos(){
        return $this->hasMany(Recurso::class);
    }

    public function versiones(){
        return $this->hasMany(Version::class)->orderByDesc('created_at');
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function ciclo(){
        return $this->belongsTo(Ciclo::class);
    }

    public function comentarios(){
        return $this->hasMany(Comentario::class)->orderByDesc('created_at');
    }

}
