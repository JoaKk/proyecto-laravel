<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comentario extends Model
{
    protected $table = 'comentarios';
    protected $fillable = ['proyecto_id','user_id','descripcion'];
    use HasFactory;

    
    public function getUsuario(){
        $usuario = User::where('id',$this->user_id)->first();
        return $usuario;
    }
}
