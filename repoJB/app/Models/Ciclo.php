<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ciclo extends Model
{
    protected $table = 'ciclos';
    use HasFactory;

    public function institutos(){
        return $this->belongsToMany(Instituto::class);
    }

    public function getRouteKeyName()
    {
        return 'slug';
    } 
    
    public function proyectos(){
        return $this->hasMany(Proyecto::class);
    }
}
