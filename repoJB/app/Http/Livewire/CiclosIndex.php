<?php

namespace App\Http\Livewire;

use App\Models\Ciclo;
use Livewire\Component;
use Livewire\WithPagination;

class CiclosIndex extends Component
{
    use WithPagination;
    protected $paginationTheme = "bootstrap";
    public $search;

    public function updatingSearch()
    {
        $this->resetPage();
    }
    public function render()
    {
        $ciclos = Ciclo::where('nombre', 'LIKE', '%' . $this->search . '%')
            ->orderBy('nombre')
            ->paginate(10);
        return view('livewire.ciclos-index', compact('ciclos'));
    }
}
