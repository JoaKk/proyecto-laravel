<?php

namespace App\Http\Livewire;

use App\Models\Ciclo;
use App\Models\Proyecto;
use Livewire\Component;
use Livewire\WithPagination;

class ProyectosIndex extends Component
{

    use WithPagination;
    protected $paginationTheme = "bootstrap";
    public $search;

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        $proyectos = Proyecto::latest('created_at')
            ->where('nombre', 'LIKE', '%' . $this->search . '%')
            ->paginate(10);
        return view('livewire.proyectos-index', compact('proyectos'));
    }
}
