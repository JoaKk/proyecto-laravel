<?php

namespace App\Http\Livewire;

use App\Models\Ciclo;
use App\Models\Proyecto;
use Livewire\Component;
use Livewire\WithPagination;


class ProyectosPersonales extends Component
{

    use WithPagination;
    protected $paginationTheme = "bootstrap";
    public $busqueda;

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        $proyectos = Proyecto::latest('created_at')
            ->where('user_id',auth()->user()->id)
            ->paginate(10);
        return view('livewire.proyectos-personales', compact('proyectos'));
    }
}
