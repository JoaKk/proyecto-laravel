<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProyectoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $proyecto = $this->route()->parameter('proyecto');

        $rules = [
            'nombre' => 'required',
            'slug' => 'required|unique:proyectos',
            //'publico' => 'required|in:0,1',
            'descripcion' => 'required',
            'ruta_logo' => 'image',
            'ruta_fichero' => 'required|mimes:rar,zip',
            'ruta_documentacion' => 'required|mimes:pdf,doc'
        ];
        // Cambiar reglas de validacion si existe el proyecto, por ejemplo al editar
        if ($proyecto) {
            $rules['slug'] = 'required|unique:proyectos,slug,' . $proyecto->id; //Permite introducir el mismo slug ya que pertenece al mismo proyecto
            $rules['ruta_fichero'] = 'mimes:rar,zip';
            $rules['ruta_documentacion'] = 'mimes:pdf,doc';
        }
        return $rules;
    }
}
