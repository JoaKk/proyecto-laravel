<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProyectoRequest;
use App\Models\Ciclo;
use App\Models\Comentario;
use App\Models\Proyecto;
use App\Models\Recurso;
use App\Models\TipoRecurso;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use ZipArchive;
use File;

class ProyectoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //$proyectos = Proyecto::all();
        return view('proyectos.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ciclos = Ciclo::orderBy('nombre')->pluck('nombre', 'id');
        return view('proyectos.create', compact('ciclos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProyectoRequest $request)
    {
        $publico = 1;
        if (!$request->has('publico')) {
            $publico = 0;
        }
        $proyecto = new Proyecto();
        $proyecto->fill($request->all());
        $proyecto->publico = $publico;
        $proyecto->save();
        if ($request->file('ruta_logo')) {
            $proyecto->ruta_logo = Storage::put('recursos/' . $proyecto->id, $request->file('ruta_logo'));
            $proyecto->save();
        }

        if ($request->file('ruta_fichero')) {
            $urlFile = Storage::put('recursos/' . $proyecto->id, $request->file('ruta_fichero'));
            $proyecto->recursos()->create([
                'ruta_fichero' => $urlFile,
                'tipo_id' => TipoRecurso::all()->random()->id
            ]);
        }
        if ($request->file('ruta_documentacion')) {
            $urlFile = Storage::put('recursos/' . $proyecto->id, $request->file('ruta_documentacion'));
            $proyecto->recursos()->create([
                'ruta_fichero' => $urlFile,
                'tipo_id' => TipoRecurso::all()->random()->id
            ]);
        }
        return redirect()->route('proyectos.show', $proyecto)->with('createInfo', 'Proyecto creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Proyecto $proyecto)
    {
        return view('proyectos.show', compact('proyecto'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Proyecto $proyecto)
    {
        $this->authorize('author', $proyecto);
        $ciclos = Ciclo::pluck('nombre', 'id');
        return view('proyectos.edit', compact('proyecto', 'ciclos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProyectoRequest $request, Proyecto $proyecto)
    {
        $this->authorize('author', $proyecto);
        $url_logo_delete = $proyecto->ruta_logo;
        $proyecto->update($request->all());
        if ($request->file('ruta_logo')) {
            Storage::delete(($url_logo_delete));
            $proyecto->update([
                'ruta_logo' =>  Storage::put('recursos/' . $proyecto->id, $request->file('ruta_logo'))
            ]);
        }

        if ($request->file('ruta_fichero')) {
            $url_fichero = Storage::put('recursos/' . $proyecto->id, $request->file('ruta_fichero'));
            $recurso = $proyecto->recursos->whereIn('tipo_id', [1, 2])->first();
            if ($recurso) {
                Storage::delete($recurso->ruta_fichero);

                $recurso->update([
                    'ruta_fichero' => $url_fichero
                ]);
            }
        }
        if ($request->file('ruta_documentacion')) {
            $url_fichero = Storage::put('recursos/' . $proyecto->id, $request->file('ruta_documentacion'));
            $recurso = $proyecto->recursos->whereIn('tipo_id', [3, 4])->first();
            if ($recurso) {
                Storage::delete($recurso->ruta_fichero);
                $recurso->update([
                    'ruta_documentacion' => $url_fichero
                ]);
            }
        }
        return redirect()->route('proyectos.edit', $proyecto)->with('updateInfo', 'Proyecto actualizado con éxito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function download(Proyecto $proyecto)
    {
        $zip = new ZipArchive;
        $fileName = $proyecto->nombre . ".rar";
        Storage::makeDirectory('proyectos/' . $proyecto->id);
        if ($zip->open(public_path('storage/proyectos/' . $proyecto->id . '/' . $fileName), ZipArchive::CREATE) === true) {
            $files = File::files(public_path('storage/recursos/' . $proyecto->id));
            foreach ($files as $key => $value) {
                $relativeNameInZipFile = basename($value);
                $zip->addFile($value, $relativeNameInZipFile);
            }
            $zip->close();
        }
        $proyecto->update([
            'descargas' => $proyecto->descargas + 1
        ]);
        return response()->download(public_path('storage/proyectos/' . $proyecto->id . '/' . $fileName));
    }

    public function personales()
    {
        $proyectos = Proyecto::all();
        return view('proyectos.personales', compact('proyectos'));
    }

    public function destroy(Proyecto $proyecto)
    {
        $this->authorize('author', $proyecto);
        $proyecto->delete();
        Storage::deleteDirectory('proyectos/' . $proyecto->id);
        Storage::deleteDirectory('recursos/' . $proyecto->id);
        return redirect()->route('proyectos.personales')->with('destroyInfo', 'El proyecto se ha eliminado con éxito');
    }

    public function comentar(Request $request, Proyecto $proyecto)
    {
        $comentario = Comentario::create([
            'proyecto_id' => $proyecto->id,
            'user_id' => auth()->user()->id,
            'descripcion' => $request->comentario
        ]);
        return redirect()->route('proyectos.show', $proyecto)->with('comentarioInfo', 'El comentario se ha insertado con éxito');
    }

    public function eliminarComentario($id)
    {
        $comentario = Comentario::where('id', $id)->first();
        $proyecto = Proyecto::where('id', $comentario->proyecto_id)->first();
        $comentario->delete();
        return redirect()->route('proyectos.show', $proyecto)->with('eliminarComentarioInfo', 'El comentario ha sido eliminado con éxito');
    }

    public function descargarLogo(Proyecto $proyecto)
    {
        return Storage::download($proyecto->ruta_logo);
    }

    public function step()
    {
        $ciclos = Ciclo::pluck('nombre', 'id');
        return view('proyectos.step', compact('ciclos'));
    }
}
